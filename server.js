const express = require('express');
const bodyParser = require('body-parser');
const app = express();

const dbConfig = require('./config/database.config.js');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;


mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

require('./app/routes/classs.routes.js')(app)
require('./app/routes/student.routes.js')(app)
require('./app/routes/app.routes.js')(app)

app.listen(3000, () => {
    console.log("Server is listening on port 3000");
});