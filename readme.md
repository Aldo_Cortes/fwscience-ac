# FW science

Test from Aldo Cortés to hopefully collaborate with fw science

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
Node JS v10.2.1
NPM v6.2.0
```

### Installing

A step by step series of examples that tell you how to get a development env running


```
npm i
mongod (if installed)
node server.js
```

## Authors

* **Aldo Cortés** - *Initial work* - [email](mailto:aldo-cortes@outlook.com)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details