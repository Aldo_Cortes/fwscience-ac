module.exports = (app) => {
    const classes = require('../controllers/classes.controller.js');
    app.post(   '/class', classes.create);
    app.get(    '/classes', classes.findAll);
    app.get(    '/class/:classID', classes.findOne);
    app.put(    '/class/:classID', classes.update);
    app.delete( '/class/:classID', classes.delete);
}