module.exports = (app) => {
    const students = require('../controllers/students.controller.js');
    app.post(   '/student', students.create);
    app.get(    '/students', students.findAll);
    app.get(    '/student/:studentID', students.findOne);
    app.put(    '/student/:studentID', students.update);
    app.delete( '/student/:studentID', students.delete);
}