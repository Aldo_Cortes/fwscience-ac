module.exports = (app) => {
    const assignment = require('../controllers/assignment.controller.js');
    app.post(   '/assign', assignment.toggle);
    app.get(   '/assignments', assignment.findAll);
    app.post(   '/search/user', assignment.searchByStudentName);
    app.post(   '/search/class', assignment.searchByClassName);
    app.get(   '/search/assignments-per-student/:student', assignment.retrieveAssignmentsFor);
}