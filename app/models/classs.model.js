const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ClassSchema = Schema({
    code: String,
    title: String,
    description: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Classs', ClassSchema);