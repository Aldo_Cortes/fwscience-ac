const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const AssignmentSchema = Schema({
    student: { type: Schema.Types.ObjectId, ref: 'students' },
    class: { type: Schema.Types.ObjectId, ref: 'classes' }
}, {
    timestamps: true
});

module.exports = mongoose.model('Assignment', AssignmentSchema);