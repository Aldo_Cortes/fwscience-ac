const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StudentSchema = Schema({
    first_name: String,
    last_name: String
}, {
    timestamps: true
});

module.exports = mongoose.model('Student', StudentSchema);