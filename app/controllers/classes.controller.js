const mongoose = require('mongoose');
const Classs = require('../models/classs.model.js');

exports.create = (req, res) => {
    const title = req.body.title;
    const description = req.body.description;
    
    if(!title || !description) {
        return res.status(400).send({
            message: "Class title and description can not be empty"
        });
    }

    const uuid = new mongoose.Types.ObjectId();
    const classs = new Classs({
        code: uuid,
        title: title, 
        description: description
    });

    classs.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Class."
        });
    });
};

exports.findAll = (req, res) => {
    Classs.find()
    .then(classss => {
        res.send(classss);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving Classes."
        });
    });
};

exports.findOne = (req, res) => {
    Classs.findById(req.params.classID)
    .then(classs => {
        if(!classs) {
            return res.status(404).send({
                message: "Class not found with id " + req.params.classID
            });            
        }
        res.send(classs);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Class not found with id " + req.params.classID
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Class with id " + req.params.classID
        });
    });
};

exports.update = (req, res) => {
    const title = req.body.title;
    const description = req.body.description;
    
    if(!title || !description) {
        return res.status(400).send({
            message: "Class title and description can not be empty"
        });
    }

    Classs.findByIdAndUpdate(req.params.classID, {
        title: title, 
        description: description
    }, {new: true})
    .then(classs => {
        if(!classs) {
            return res.status(404).send({
                message: "Class not found with id " + req.params.classID
            });
        }
        res.send(classs);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Class not found with id " + req.params.classID
            });                
        }
        return res.status(500).send({
            message: "Error updating Class with id " + req.params.classID
        });
    });
};

exports.delete = (req, res) => {
    Classs.findByIdAndRemove(req.params.classID)
    .then(classs => {
        if(!classs) {
            return res.status(404).send({
                message: "Class not found with id " + req.params.classID
            });
        }
        res.send({message: "Class deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Class not found with id " + req.params.classID
            });                
        }
        return res.status(500).send({
            message: "Could not delete Class with id " + req.params.classID
        });
    });
};