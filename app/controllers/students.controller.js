const Student = require('../models/student.model.js');
const mongoose = require('mongoose');

exports.create = (req, res) => {
    const firstName = req.body.name;
    const lastName = req.body.lastName;
    
    if(!firstName || !lastName) {
        return res.status(400).send({
            message: "Student names can not be empty"
        });
    }

    const student = new Student({
        first_name: firstName, 
        last_name: lastName
    });

    student.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Student."
        });
    });
};

exports.findAll = (req, res) => {
    Student.find()
    .then(students => {
        res.send(students);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving students."
        });
    });
};

exports.findOne = (req, res) => {
    Student.findById(req.params.studentID)
    .then(student => {
        if(!student) {
            return res.status(404).send({
                message: "Student not found with id " + req.params.studentID
            });            
        }
        res.send(student);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Student not found with id " + req.params.studentID
            });                
        }
        return res.status(500).send({
            message: "Error retrieving student with id " + req.params.studentID
        });
    });
};

exports.update = (req, res) => {
    const firstName = req.body.name;
    const lastName = req.body.lastName;
    
    if(!firstName || !lastName) {
        return res.status(400).send({
            message: "Student names can not be empty"
        });
    }

    Student.findByIdAndUpdate(req.params.studentID, {
        first_name: firstName, 
        last_name: lastName
    }, {new: true})
    .then(student => {
        if(!student) {
            return res.status(404).send({
                message: "Student not found with id " + req.params.studentID
            });
        }
        res.send(student);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Student not found with id " + req.params.studentID
            });                
        }
        return res.status(500).send({
            message: "Error updating student with id " + req.params.studentID
        });
    });
};

exports.delete = (req, res) => {
    Student.findByIdAndRemove(req.params.studentID)
    .then(student => {
        if(!student) {
            return res.status(404).send({
                message: "Student not found with id " + req.params.studentID
            });
        }
        res.send({message: "Student deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Student not found with id " + req.params.studentID
            });                
        }
        return res.status(500).send({
            message: "Could not delete student with id " + req.params.studentID
        });
    });
};