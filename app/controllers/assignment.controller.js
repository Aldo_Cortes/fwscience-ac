const Assignment = require('../models/assignment.model.js');
const Classs = require('../models/classs.model.js');
const Student = require('../models/student.model.js');

exports.searchByStudentName = (req, res) => {
    const studentName = req.body.name || "";
    const studentLastName = req.body.lastName || "";
    
    Student.find({
        first_name: { $regex: studentName, $options: 'i' },
        last_name: { $regex: studentLastName, $options: 'i' }
    })
    .select('-updatedAt -createdAt -__v')
    .then(matches => {
        res.send(matches);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Students not found with name " + studentName
            });                
        }
        return res.status(500).send({
            message: "Error processing students "
        });
    });
};

exports.searchByClassName = () => {

};

exports.retrieveAssignmentsFor = (req, res) => {
    const student = req.params.student;
    Assignment.find({
        userID: student
    })
    .select('-updatedAt -createdAt -__v')
    .populate('student')
    .populate('class', 'title')
    .then(matches => {
        res.send(matches);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Assignments not found for student with ID " + student
            });                
        }
        return res.status(500).send({
            message: "Error processing assignments"
        });
    });
};

exports.findAll = (req, res) => {
    Assignment.find()
    .then(assignments => {
        res.send(assignments);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving assignments."
        });
    });
};

exports.toggle = (req, res) => {
    const userID_   = req.body.user;
    const classID_  = req.body.class;

    Assignment.find({
        $and: [
               { userID : userID_  },
               { classID: classID_ }
            ]
      })
    .then(assignment => {
        const assign = new Assignment({
            userID: userID_,
            classID: classID_
        });
        if(!assignment.length) {
            assign.save()
            .then(data => {
                res.send(data);
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while creating the assignment."
                });
            });
        } else {
            Assignment.findByIdAndRemove(assignment[0]._id)
            .then(toDelete => {
                res.send({message: "Assignment deleted successfully!"});
            }).catch(err => {
                if(err.kind === 'ObjectId' || err.name === 'NotFound') {
                    return res.status(404).send({
                        message: "Assignment not found with id " + req.params.classID
                    });                
                }
                return res.status(500).send({
                    message: "Could not delete Class with id " + req.params.classID
                });
            });
        }
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Class not found with id " + req.params.classID
            });                
        }
        return res.status(500).send({
            message: "Error processing assignment "
        });
    });
};

exports.getAssignaments = (req, res) => {
    const user = req.body.user;
    const classs  = req.body.class;
    
    if(!user || !classs) {
        return res.status(400).send({
            message: "Not enough info."
        });
    }

    Assignment.findOne({
        $and: [
               { userID : user },
               { classID: { $lt: new Date('01/01/1945') } }
             ]
    });

    Assignment.findByIdAndUpdate(req.params.classID, {
        title: title, 
        description: description
    }, {new: true})
    .then(classs => {
        if(!classs) {
            return res.status(404).send({
                message: "Class not found with id " + req.params.classID
            });
        }
        res.send(classs);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Class not found with id " + req.params.classID
            });                
        }
        return res.status(500).send({
            message: "Error updating Class with id " + req.params.classID
        });
    });
};

exports.delete = (req, res) => {
    Classs.findByIdAndRemove(req.params.classID)
    .then(classs => {
        if(!classs) {
            return res.status(404).send({
                message: "Class not found with id " + req.params.classID
            });
        }
        res.send({message: "Class deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Class not found with id " + req.params.classID
            });                
        }
        return res.status(500).send({
            message: "Could not delete Class with id " + req.params.classID
        });
    });
};